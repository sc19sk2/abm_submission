extensions [ gis ]

globals [
  country-dataset
  counter
  change
]

patches-own [ patch-name patch-seats patch-bias ]

breed [ scotland scottish ]     ;; scotland 59 seats
breed [ england english ]       ;; england 533 seats
breed [ wales welsh ]           ;; wales 40 seats
breed [ ireland irish ]         ;; ireland 18 seats

undirected-link-breed [ scotland-links scotland-link ]
undirected-link-breed [ england-links england-link ]
undirected-link-breed [ wales-links wales-link ]
undirected-link-breed [ ireland-links ireland-link ]

turtles-own [
  total-bias
  vote
  opinion
  opinion-list
  polarisation?
  previous
  influenced-opinion
]

to setup

  clear-all

  ask patches [set pcolor sky + 4]
  set-default-shape turtles "circle"

  setup-map
  setup-network

  reset-ticks

end

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Map Setup Procedures ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

to setup-map

  ;; load gis shape file and set to netlogo world
  set country-dataset gis:load-dataset "country_layer.shp"
  gis:set-world-envelope (gis:envelope-of country-dataset)
  gis:draw country-dataset 1
  gis:set-drawing-color black

  ;; assign gis FeatureValue properties to each patch
  gis:apply-coverage country-dataset "COUNTRY" patch-name
  gis:apply-coverage country-dataset "SEATS" patch-seats

  ;; ask patches to set color based on patch country name ('patch-name')
  ask patches [
    if patch-name = "scotland" [ set pcolor blue ]
    if patch-name = "england"  [ set pcolor white ]
    if patch-name = "wales"    [ set pcolor red ]
    if patch-name = "ireland"  [ set pcolor lime ]
  ]

end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Network Setup Procedures ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to setup-network

  ;; inititate turtle counter to track turtle id numbers
  set counter 0
  ;; loop through each gis FeatureValue to:
  foreach gis:feature-list-of country-dataset [ this-country ->
    ;; assign gis:properties to each patch
    let country (gis:property-value this-country "Country")
    let seats (gis:property-value this-country "Seats")
    ;; find patch located in middle of each country's gis polygon
    let center gis:location-of gis:centroid-of this-country
    ;; ask these center patches to sprout initial nodes
    ask patch (first center + 3) last center [
      if country = "scotland" [ sprout-scotland 2 [ set size 2.0 set color yellow fd 5 ] ]
      if country = "england"  [ sprout-england  2 [ set size 2.0 set color yellow fd 5 ] ]
      if country = "wales"    [ sprout-wales    2 [ set size 2.0 set color yellow fd 5 ] ]
      if country = "ireland"  [ sprout-ireland  2 [ set size 2.0 set color yellow fd 5 ] ]
    ]
    ;; then ask turtles to create link between the two initial nodes
    ask turtle counter [
      if country = "scotland" [create-scotland-link-with scottish (counter + 1) ]
      if country = "england"  [create-england-link-with english   (counter + 1) ]
      if country = "wales"    [create-wales-link-with welsh       (counter + 1) ]
      if country = "ireland"  [create-ireland-link-with irish     (counter + 1) ]
   ]
    ;; update turtle counter
    set counter counter + seats
    ;; find node partner by calling partner procedure and add nodes to network
    ;; according to number of seats for each country (layout network using spring layout)
   repeat seats - 2 [
      if country = "scotland" [ make-node scotland find-partner scotland-links ]
      if country = "england"  [ make-node england find-partner england-links ]
      if country = "wales"    [ make-node wales find-partner wales-links ]
      if country = "ireland"  [ make-node ireland find-partner ireland-links ]
    ]
  ]

end

;; create breed-nodes according to country and
;; link to partners chosen from partner procedure
to make-node [breed-country old-node]

  if breed-country = scotland [
    create-scotland 1 [
      if old-node != nobody [
        create-scotland-link-with old-node [set color yellow ]
        move-to old-node
        fd 6
      ]
    ]
  ]
  if breed-country = england [
    create-england 1 [
      if old-node != nobody [
        create-england-link-with old-node [set color yellow ]
        move-to old-node
        fd 8
      ]
    ]
  ]
  if breed-country = wales [
    create-wales 1 [
      if old-node != nobody [
        create-wales-link-with old-node [set color yellow ]
        move-to old-node
        fd 5
      ]
    ]
  ]
  if breed-country = ireland [
    create-ireland 1 [
      if old-node != nobody [
        create-ireland-link-with old-node [set color yellow ]
        move-to old-node
        fd 5
      ]
    ]
  ]
  ask turtles [ set size 2 set color yellow ]
  ask links [ set color yellow ]

end

;; PREFERENTIAL ATTACHMENT MODEL:
;; heart of the preferential attachment model is selecting random links
;; to choose a partner node depending on how many links a node already has
to-report find-partner [link-breed-country]

  report [one-of both-ends] of one-of link-breed-country

end

;; spring-layout based on center of gis polygon
to layout [breed-country link-breed-country center-country]

  repeat 5 [
    ;; additional nodes reduce inputs to the layout-spring and allow nodes
    ;; to fit into the same amount of space in the gis polygon
    let factor sqrt count breed-country
    layout-spring breed-country link-breed-country (1 / factor)  (12 / factor)  (1 / factor)
    display  ;; to smooth animation
  ]
  ;; nodes are continuously centered to each gis polygon
  ask breed-country [
    facexy first center-country last center-country
    fd (distancexy first center-country last center-country) / adjust-distance breed-country
  ]

end

;; adjust distances between nodes in the layout-spring depending on the number of nodes
to-report adjust-distance [breed-country]

  if breed-country = scotland [ report 300 ]
  if breed-country = england  [ report 400 ]
  if breed-country = wales    [ report 25 ]
  if breed-country = ireland  [ report 50 ]

end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Regional-Bias Procedures ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to setup-regional-bias

  ;; assign initial bias to countries according to pct bias
  ask patches [
    if patch-name = "scotland" [
      ifelse random 100 > bias-scotland-pct
        [ set patch-bias 0 ]
        [ set patch-bias 1 ]
    recolor-patch "scotland"
  ]
  if patch-name = "england" [
    ifelse random 100 > bias-england-pct
      [ set patch-bias 0 ]
      [ set patch-bias 1 ]
    recolor-patch "england"
  ]
  if patch-name = "wales" [
    ifelse random 100 > bias-wales-pct
      [ set patch-bias 0 ]
      [ set patch-bias 1 ]
    recolor-patch "wales"
  ]
  if patch-name = "ireland" [
    ifelse random 100 > bias-ireland-pct
      [ set patch-bias 0 ]
      [ set patch-bias 1 ]
    recolor-patch "ireland"
  ]
]

end

to influence-regional-bias

  ;; keep track of patch changes to stop running if no more changes
  let any-patches-changed? false
  ;; sum votes of patch neighbors to compute 'total' before setting regional bias for each agent
  ask turtles [
    set total-bias (sum [patch-bias] of neighbors)
  ]
  ;; SET INITIAL VOTE: set initial vote according to regional bias
  ask turtles [
    let previous-patch-bias patch-bias
    ;; set vote to 1 if more than half of patch neighbors are in favor of Brexit
    if total-bias > 4 [ set vote 1 set patch-bias 1 ]
    recolor
    if patch-bias != previous-patch-bias [ set any-patches-changed? true ]
  ]
  ;; stop running if no more patches changed
  if not any-patches-changed? [ stop ]
  tick

end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Social-Network Procedures ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to setup-social-network

  ;; SET OPINIONS: randomly assign opinions to agents according to initial vote and add to first element of opinion-list
  ask turtles [
    ;; set opinion 0 to 50 for vote = 0 and above 50 for vote = 1
    ifelse vote = 0 [ set opinion random 50 ] [ set opinion random 50 + 50 ]
    ;; initialize turtle variables to avoid any run errors in different versions of NetLogo
    set opinion-list (list opinion opinion)
    set influenced-opinion opinion
    set polarisation? false
    recolor
  ]

  ;; SET polarisation: agents with opinions in the polarisation range do not change opinions
  initialize-polarisation type-s polarisation-scotland scotland
  initialize-polarisation type-e polarisation-england england
  initialize-polarisation type-w polarisation-wales wales
  initialize-polarisation type-i polarisation-ireland ireland

  ;; SET NETWORK OPINION: calculate network opinion using similar opinions in network and add to opinion-list (initiate array)
  ;; SET INFLUENCED-OPINION: set inflluence-opinion as mean of opinion-list
  ; set opinion-list and set influenced opinion (consists of current opinion and network opinion)
  initialize-opinion-list-and-initialize-influenced-opinion epsilon-scotland scotland
  initialize-opinion-list-and-initialize-influenced-opinion epsilon-england england
  initialize-opinion-list-and-initialize-influenced-opinion epsilon-wales wales
  initialize-opinion-list-and-initialize-influenced-opinion epsilon-ireland ireland

end

to initialize-polarisation [ type-country polarisation-country breed-country]

  ask breed-country [
    if (type-country = "two-way") [ set polarisation? ((opinion < polarisation-country)) or (opinion > (100 - polarisation-country)) ]
    if (type-country = "one-way") [ set polarisation? (opinion < polarisation-country)]
  ]

end

to initialize-opinion-list-and-initialize-influenced-opinion [ epsilon-country breed-country ]

  ask breed-country [
    let aggregate-sum (filter-opinion opinion epsilon-country breed-country)
    if empty? aggregate-sum [ set aggregate-sum (list opinion) ]
    ;; initiate opinion-list with network opinion calculation
    set opinion-list lput (mean aggregate-sum) opinion-list
    ;; COMMENT OUT PRINT TO SPEED UP RUN TIMES
    print (word who " " breed-country " " influenced-opinion)
    ;; set influenced-opinion as mean of opinion-list but use current opinion if polarisation is true
    ifelse not polarisation? [ set influenced-opinion (mean opinion-list) ] [ set influenced-opinion opinion ]
  ]

end

to influence-social-network

  set change 0
  ;; CHECK VOTE: use influenced-opinion to check vote
  ;; UPDATE VOTE: after checking for vote then update current opinion with influenced-opinion
  check-vote-and-update-opinion scotland
  check-vote-and-update-opinion england
  check-vote-and-update-opinion wales
  check-vote-and-update-opinion ireland

  ;; SET NETWORK OPINION: calculate network opinion using similar opinions after all turtles updated and add to opinion-list
  ;; SET INFLUENCED-OPINION: reset influenced-opinion as mean of newly-updated opinion-list
  update-network-opinion-and-replace-opinion-list epsilon-scotland scotland
  update-network-opinion-and-replace-opinion-list epsilon-england england
  update-network-opinion-and-replace-opinion-list epsilon-wales wales
  update-network-opinion-and-replace-opinion-list epsilon-ireland ireland

  ;; stop if no more opinions changing
  if change >= 650 [ stop ]

  tick

end

;; use influenced-opinion to set vote and reset opinion with influenced-opinion
to check-vote-and-update-opinion [ breed-country ]

   ask breed-country [
    ;; track previous opinion to check if opinions changing
    set previous opinion

    ;; COMMENT OUT PRINT TO SPEED UP RUN TIMES
    print (word who " " breed-country " " influenced-opinion)
    ;; votes change as randomizer updates
    if not polarisation? [
      let randomizer random 100
      ;; votes update in both directions
      ifelse randomizer < influenced-opinion [ set vote 1 ] [ set vote 0 ]
    ]
    recolor
    set opinion influenced-opinion
    set opinion-list replace-item 0 opinion-list opinion
    ;; increment 'change' counter if opinion not changing
    if abs(previous - opinion) < 1 [ set change change + 1 ]
  ]

end

to update-network-opinion-and-replace-opinion-list [ epsilon-country breed-country ]

  ask breed-country [
    let aggregate-sum (filter-opinion opinion epsilon-country breed-country)
    if empty? aggregate-sum [ set aggregate-sum (list opinion) ]
    ;; replace existing network opinion in opinion-list with updated network opinion calculation
    set opinion-list replace-item 1 opinion-list (mean aggregate-sum)
    ifelse not polarisation? [ set influenced-opinion (mean opinion-list) ] [ set influenced-opinion opinion ]
  ]

end

;; THE BOUNDED CONFIDENCE MODEL:
;; only sum the opinions of other agents with opinions within the epsilon value
to-report filter-opinion [ opinion-country epsilon-country breed-country ]

  report (filter [ x -> abs(x - opinion-country) < epsilon-country ] [opinion] of other breed-country)

end

;;;;;;;;;;;;;
;; Helpers ;;
;;;;;;;;;;;;;

to recolor-patch [patch-country]

  if patch-country = "scotland" [
    ifelse patch-bias = 0
      [ set pcolor 105 ]
      [ set pcolor 103 ]
  ]
  if patch-country = "england" [
    ifelse patch-bias = 0
      [ set pcolor white ]
      [ set pcolor 9 ]
  ]
  if patch-country = "wales" [
    ifelse patch-bias = 0
      [ set pcolor 15 ]
      [ set pcolor 13 ]
  ]
  if patch-country = "ireland" [
    ifelse patch-bias = 0
      [ set pcolor 65 ]
      [ set pcolor 63 ]
  ]

end

to recolor

  ifelse vote = 1
  [ set color pink + 2 ]
  [ set color yellow ]

end

;;;;;;;;;;;;;;;
;; Reporters ;;
;;;;;;;;;;;;;;;

to-report pct-vote-scotland                         ;; Pct remain: 62.0%  Pct leave: 38.0%
  report precision (count scotland with [ vote = 1 ] / count scotland * 100 ) 1
end

to-report pct-vote-england                         ;; Pct remain: 46.6%   Pct leave: 53.4%
  report precision (count england with [ vote = 1 ] / count england * 100 ) 1
end

to-report pct-vote-wales                           ;; Pct remain: 47.5%   Pct leave: 52.5%
  report precision (count wales with [ vote = 1 ] / count wales * 100 ) 1
end

to-report pct-vote-ireland                         ;; Pct remain: 55.8%   Pct leave: 44.3%
  report precision (count ireland with [ vote = 1 ] / count ireland * 100 ) 1
end

to-report pct-vote-total                           ;; Pct remain: 48.1   Pct leave: 51.9%
  report precision (count turtles with [ vote = 1 ] / count turtles * 100 ) 1
end

to-report vote-scotland                            ;; Remain: 57 (96.6%) Leave: 2 (3.4%)
  report count scotland with [ vote = 1 ]
end

to-report vote-england                             ;; Remain: 161 (30.2%) Leave: 372 (69.8%)
  report count england with [ vote = 1 ]
end

to-report vote-wales                               ;; Remain: 11 (27.5%) Leave: 29 (72.5%)
  report count wales with [ vote = 1 ]
end

to-report vote-ireland                             ;; Remain: 12 (66.7%) Leave: 6 (33.3%)
  report count ireland with [ vote = 1 ]
end

to-report vote-total                               ;; Remain: 241 (37.1%) Leave: 409 (62.9%)
  report count turtles with [ vote = 1]
end
@#$#@#$#@
GRAPHICS-WINDOW
210
10
676
477
-1
-1
2.28
1
30
1
1
1
0
0
0
1
-100
100
-100
100
0
0
1
ticks
30.0

BUTTON
684
281
862
314
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
684
315
862
348
NIL
setup-regional-bias
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
23
30
200
63
bias-scotland-pct
bias-scotland-pct
0
100
50.0
1
1
%
HORIZONTAL

SLIDER
23
64
200
97
bias-england-pct
bias-england-pct
0
100
50.0
1
1
%
HORIZONTAL

SLIDER
23
98
199
131
bias-wales-pct
bias-wales-pct
0
100
50.0
1
1
%
HORIZONTAL

SLIDER
23
132
199
165
bias-ireland-pct
bias-ireland-pct
0
100
50.0
1
1
%
HORIZONTAL

TEXTBOX
28
11
178
29
Regional bias assumptions
11
0.0
1

BUTTON
684
349
862
382
NIL
influence-regional-bias
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
688
246
838
274
Setup country, network & regional bias
11
0.0
1

TEXTBOX
687
400
837
418
Influence-social-network
11
0.0
1

BUTTON
687
417
866
450
NIL
setup-social-network
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
26
184
176
202
Social network assumptions
11
0.0
1

SLIDER
22
202
198
235
epsilon-scotland
epsilon-scotland
0
100
50.0
1
1
%
HORIZONTAL

SLIDER
22
236
198
269
epsilon-england
epsilon-england
0
100
50.0
1
1
%
HORIZONTAL

SLIDER
22
270
198
303
epsilon-wales
epsilon-wales
0
100
50.0
1
1
%
HORIZONTAL

SLIDER
22
304
198
337
epsilon-ireland
epsilon-ireland
0
100
50.0
1
1
%
HORIZONTAL

TEXTBOX
24
349
174
367
Polarisation assumptions
11
0.0
1

SLIDER
21
367
197
400
polarisation-scotland
polarisation-scotland
0
50
0.0
1
1
%
HORIZONTAL

SLIDER
21
401
197
434
polarisation-england
polarisation-england
0
50
0.0
1
1
%
HORIZONTAL

SLIDER
21
435
197
468
polarisation-wales
polarisation-wales
0
50
0.0
1
1
%
HORIZONTAL

SLIDER
21
469
197
502
polarisation-ireland
polarisation-ireland
0
50
0.0
1
1
%
HORIZONTAL

CHOOSER
261
488
437
533
type-s
type-s
"one-way" "two-way"
0

CHOOSER
454
488
629
533
type-e
type-e
"one-way" "two-way"
0

CHOOSER
261
534
437
579
type-w
type-w
"one-way" "two-way"
0

CHOOSER
454
534
629
579
type-i
type-i
"one-way" "two-way"
0

BUTTON
687
451
866
484
NIL
influence-social-network
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
687
11
837
29
Voting results (% & votes)
11
0.0
1

MONITOR
686
26
776
59
scotland (%)
pct-vote-scotland
17
1
8

MONITOR
686
60
776
93
england (%)
pct-vote-england
17
1
8

MONITOR
777
26
867
59
wales (%)
pct-vote-wales
17
1
8

MONITOR
777
60
867
93
ireland (%)
pct-vote-ireland
17
1
8

MONITOR
686
94
867
127
total (%)
pct-vote-total
17
1
8

MONITOR
686
128
776
161
scotland (seats)
vote-scotland
17
1
8

MONITOR
686
162
776
195
england (seats)
vote-england
17
1
8

MONITOR
777
128
867
161
NIL
vote-wales
17
1
8

MONITOR
777
162
867
195
NIL
vote-ireland
17
1
8

MONITOR
686
196
867
229
NIL
vote-total
17
1
8

@#$#@#$#@
# Brexit Opinion Dynamics

## WHAT IS IT?

A ***bounded confidence model*** by *Hegeselmann and Krause (2002)* applied to the United Kingdom's EU referendum of June 2016. The model simulates voting distribution by generating social networks for each region based on the number of Parliamentary constituencies using the *Barabasi-Albert preferential attachment* algorithm. The first part of the model reflects the impact of regional bias (*regional bias variable*) on Brexit voting patterns by assigning initial votes to agents according to votes of its surrounding patch neighbors. The social network influences voting outcomes by gradually aligning randomly-assigned opinions of each agent to the opinion of the network made up of agents with opinions within each respective agent's bounded-confidence level (*epsilon variable*). Hardliners are also added to the model (*polarisation variable*) by adapting *Deffuent's extremism modelling* to introduce agents that will not change their opinions despite the influence of the social network. Polarisation can be one-sided along the lower band of the opinion spectrum (*against Brexit*) or two-sided (*for and against Brexit*) along both ends of the opinion spectrum.

## HOW IT WORKS

Agents in favor of ***Leave*** are given a vote of 1 and those in favor of ***Remain*** are given a vote of 0 based on randomly-assigned opinions.

Initial votes are set according to ***regional bias assumptions*** that allocate a percentage of randomly-chosen patches that favor Brexit. Votes for agents and the patch it sits on are set to 1 if more than 4 neighboring patches (total > 4) have votes in favor of Brexit. New totals are calculated and votes are reset for every tick until equilibrium is reached.

Opinion values between 0% and 100% are then assigned to agents, with votes of 0 given random opinions of 50% and below and votes of 1 given random opinions above 50%.

The opinion of agents are influenced by the social network according to the ***bounded-confidence (epsilon) setting***. This determines the degree of homophily of opinions needed between agents before adopting opinions from the network. The opinion of agents used to determine voting decisions are formed from an agent's opinion list comprised of its current opinion and a network opinion calculated from an aggregate mean of similar opinions from the network. Agents only reflect the opinions of other agents with differing values within the epsilon setting when calculating the agent's aggregate mean of opinions for the network. During each tick, an agent's average opinion is generated from its opinion list and votes are set to 1 if an opinion value is greater than a randomly-generated number (the randomizer). New opinion lists are then updated by replacing the current opinion of all agents with existing network opinions before adding newly-calculated network opinions to opinion lists until equilibrium is reached.

The **polarisation setting sets an interval of opinions where agents will never change their opinions and reduces the number of agents willing to change their votes. A "one-way" polarisation sets this range from 0% to the polarisation setting and a "two-way" polarisation sets a lower band of 0% to the polarisation setting and upper band of 100% - the polarisation setting.

### *Variables*

***Regional bias:*** The percentage ranging between 0% and 100% of randomly-chosen patches with votes in favor of Brexit for each region. A regional bias setting of 0% means there is no initial bias towards voting for Brexit while a setting of 100% means the entire country has an initial voting intention in favor of Brexit. Agents with more than 4 neighboring patches in favor of Brexit will set their initial vote to 1.

***Epsilon:*** The bounded-confidence ranging between 0% and 100% or level of homophily needed between agents before another agent's opinion is included in network opinion's aggregate mean calculation. An epsilon setting of 0% means every agent will not accept the opinion of any other agent in the network while a setting of 100% means that every agent is open fully to accept any opinion.

***Polarisation:*** Opinion range from 0% to 50% where agents will not change their opinions. Intervals range between *0% to polarisation range* for "one-way" polarisation and *0% to polarisation range* and *100% - polarisation range* for "two-way" polarisation.

## HOW TO USE IT

### *Setup*

*Map drawn from imported gis data and regional networks generated using preferential attachment algorithm.*

### *Setup regional bias*

*Votes of 0 or 1 randomly assigned to country patches according to the percentage set in country bias selector.*

### *Influence regional bias*

*Each agent sums the votes of neighboring patches and agents/patches with totals greater than 4 resetting their votes to 1. Totals are recalculated for every tick.*

### *Setup social network*

*Agents with votes of 0 randomly assigned opinions between 0% and 50% and those with votes of 1 randomly assigned opinions above 50%. Each agent then calculates a network opinion using opinions of agents in the network with bounded-confidence levels at or above their own. Opinions and network opinions added to the opinion lists of all agents.*

### *Influence social network*

*Agents calculate the average of opinions and network opinions in each agent's opinion-list. Any agents with an average above a randomly-generated number set their votes to 1. For every tick, opinion lists replace current opinions with network opinions and use newly-calculated network opinions to recalculate averages of opinion lists until equilibrium is reached. When a polarisation range is set, agents with any opinions within the range(s) will not change their votes.*

## CREDITS AND REFERENCES

- Wilensky, U. (2005). NetLogo Preferential Attachment model. http://ccl.northwestern.edu/netlogo/models/PreferentialAttachment. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

- Wilensky, U. (1998). NetLogo Voting model. http://ccl.northwestern.edu/netlogo/models/Voting. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

- Wilensky, U. (2003). NetLogo Ising model. http://ccl.northwestern.edu/netlogo/models/Ising. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

- Lorenz, J. (2012). NetLogo BC model. http://ccl.northwestern.edu/netlogo/models/community/bc. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
