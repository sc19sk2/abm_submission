## How to Use

Country shapefiles are needed to run this NetLogo model so it cannot be run on NetLogo Web.
Please download NetLogo to run the model:
https://ccl.northwestern.edu/netlogo/download.shtml


# Brexit Opinion Dynamics

## WHAT IS IT?

A ***bounded confidence model*** by *Hegeselmann and Krause (2002)* applied to the United Kingdom's EU referendum of June 2016. The model simulates voting distribution by generating social networks for each region based on the number of Parliamentary constituencies using the *Barabasi-Albert preferential attachment* algorithm. The first part of the model reflects the impact of regional bias (*regional bias variable*) on Brexit voting patterns by assigning initial votes to agents according to votes of its surrounding patch neighbors. The social network influences voting outcomes by gradually aligning randomly-assigned opinions of each agent to the opinion of the network made up of agents with opinions within each respective agent's bounded-confidence level (*epsilon variable*). Hardliners are also added to the model (*polarisation variable*) by adapting *Deffuent's extremism modelling* to introduce agents that will not change their opinions despite the influence of the social network. Polarisation can be one-sided along the lower band of the opinion spectrum (*against Brexit*) or two-sided (*for and against Brexit*) along both ends of the opinion spectrum.

## HOW IT WORKS

Agents in favor of ***Leave*** are given a vote of 1 and those in favor of ***Remain*** are given a vote of 0 based on randomly-assigned opinions.

Initial votes are set according to ***regional bias assumptions*** that allocate a percentage of randomly-chosen patches that favor Brexit. Votes for agents and the patch it sits on are set to 1 if more than 4 neighboring patches (total > 4) have votes in favor of Brexit. New totals are calculated and votes are reset for every tick until equilibrium is reached.

Opinion values between 0% and 100% are then assigned to agents, with votes of 0 given random opinions of 50% and below and votes of 1 given random opinions above 50%.

The opinion of agents are influenced by the social network according to the ***bounded-confidence (epsilon) setting***. This determines the degree of homophily of opinions needed between agents before adopting opinions from the network. The opinion of agents used to determine voting decisions are formed from an agent's opinion list comprised of its current opinion and a network opinion calculated from an aggregate mean of similar opinions from the network. Agents only reflect the opinions of other agents with differing values within the epsilon setting when calculating the agent's aggregate mean of opinions for the network. During each tick, an agent's average opinion is generated from its opinion list and votes are set to 1 if an opinion value is greater than a randomly-generated number (the randomizer). New opinion lists are then updated by replacing the current opinion of all agents with existing network opinions before adding newly-calculated network opinions to opinion lists until equilibrium is reached.

The ***polarisation setting*** sets an interval of opinions where agents will never change their opinions and reduces the number of agents willing to change their votes. A "one-way" polarisation sets this range from 0% to the polarisation setting and a "two-way" polarisation sets a lower band of 0% to the polarisation setting and upper band of 100% - the polarisation setting.

### *Variables*

***Regional bias:*** The percentage ranging between 0% and 100% of randomly-chosen patches with votes in favor of Brexit for each region. A regional bias setting of 0% means there is no initial bias towards voting for Brexit while a setting of 100% means the entire country has an initial voting intention in favor of Brexit. Agents with more than 4 neighboring patches in favor of Brexit will set their initial vote to 1.

***Epsilon:*** The bounded-confidence ranging between 0% and 100% or level of homophily needed between agents before another agent's opinion is included in network opinion's aggregate mean calculation. An epsilon setting of 0% means every agent will not accept the opinion of any other agent in the network while a setting of 100% means that every agent is open fully to accept any opinion.

***Polarisation:*** Opinion range from 0% to 50% where agents will not change their opinions. Intervals range between *0% to polarisation range* for "one-way" polarisation and *0% to polarisation range* and *100% - polarisation range* for "two-way" polarisation.

## HOW TO USE IT

### *Setup*

*Map drawn from imported gis data and regional networks generated using preferential attachment algorithm.*

### *Setup regional bias*

*Votes of 0 or 1 randomly assigned to country patches according to the percentage set in country bias selector.*

### *Influence regional bias*

*Each agent sums the votes of neighboring patches and agents/patches with totals greater than 4 resetting their votes to 1. Totals are recalculated for every tick.*

### *Setup social network*

*Agents with votes of 0 randomly assigned opinions between 0% and 50% and those with votes of 1 randomly assigned opinions above 50%. Each agent then calculates a network opinion using opinions of agents in the network with bounded-confidence levels at or above their own. Opinions and network opinions added to the opinion lists of all agents.*

### *Influence social network*

*Agents calculate the average of opinions and network opinions in each agent's opinion-list. Any agents with an average above a randomly-generated number set their votes to 1. For every tick, opinion lists replace current opinions with network opinions and use newly-calculated network opinions to recalculate averages of opinion lists until equilibrium is reached. When a polarisation range is set, agents with any opinions within the range(s) will not change their votes.*

## CREDITS AND REFERENCES

- Wilensky, U. (2005). NetLogo Preferential Attachment model. http://ccl.northwestern.edu/netlogo/models/PreferentialAttachment. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

- Wilensky, U. (1998). NetLogo Voting model. http://ccl.northwestern.edu/netlogo/models/Voting. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

- Wilensky, U. (2003). NetLogo Ising model. http://ccl.northwestern.edu/netlogo/models/Ising. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.

- Lorenz, J. (2012). NetLogo BC model. http://ccl.northwestern.edu/netlogo/models/community/bc. Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL.
